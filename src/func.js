const getSum = (str1, str2) => {
 
  let pattern = new RegExp('^[0-9]+$');
  
   if (!str1){
    str1 = '0';
   }
   if (!str2 ){
    str2 = '0';
   } 
  
  if (typeof str1 == 'object'|| typeof str2 == 'object'|| Array.isArray(str1)|| Array.isArray(str2)||
   typeof str1 == 'number'|| typeof str2 == 'number'|| typeof str1 != 'string'|| typeof str2 != 'string' || !pattern.test(str1)|| !pattern.test(str2)) {
    return false;    
  } 
      
  let sum = Number(str1)+ Number(str2)
  return String(sum);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countP = 0;
  let countC = 0;
  
  for (let i in listOfPosts) {
   

    if (listOfPosts[i].author === authorName && listOfPosts[i].post) {

      countP++;

    }
    if (listOfPosts[i].comments) {
      for (let index in listOfPosts[i].comments) {
        if (listOfPosts[i].comments[index].author === authorName) {
          countC++;
         
       }
    }
    
  }
}
  return 'Post:' + countP +',comments:' + countC;

};

const tickets=(people)=> {
   let count25Cash = 0;
  let count50Cash = 0;
  
  for (let i = 0; i < people.length; i++) {
   if (people[i] == 25 ) {
    
     count25Cash ++;
   }
    if (people[i] == 50) {
      
      if (count25Cash == 0) {
        
        return 'NO'
      } else {
        count25Cash --;
        count50Cash++;
        
      }
    }
    if (people[i] == 100 ) {
     
      if (count50Cash >=1 && count25Cash >= 1 ) {
       
          count25Cash --;
          count50Cash --;
          
       } 
       else if (count50Cash == 0 && count25Cash >= 3) {
        count25Cash = count25Cash - 3;
    }
    else {
        return "NO";
    }
  }
  }
  return "YES";
}; 


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
